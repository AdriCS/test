#!/bin/bash

cd build/
#rm -rf build/*
#cmake -DCMAKE_BUILD_TYPE=Debug ..
#make mytest VERBOSE=1
make mytest_test VERBOSE=1
lcov --directory . --zerocounters
lcov -c -i -d . -o COV.base
../bin/mytest
../bin/mytest_test
lcov --directory . --capture --output-file COV.info
lcov -a COV.base -a COV.info --output-file COV.total
lcov -r COV.total /usr/include/\* $(pwd)/gtest/\* $(dirname $(pwd))/src/main.cpp $(dirname $(pwd))/test/\* -o COV.cleaned
rm -rf COV; genhtml -o COV COV.cleaned
gcovr -k --object-directory=CMakeFiles/ -r .. -e '.*gtest.*' -e '.*src/main.cpp' --summary
gcovr --object-directory=$(pwd)/CMakeFiles -r .. -e '.*gtest.*' -e '.*src/main.cpp' -e '.*_test.cpp' --html --html-details -o salida.html
