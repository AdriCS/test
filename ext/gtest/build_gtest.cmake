include(ExternalProject)

ExternalProject_Add(build_gtest
   GIT_REPOSITORY https://github.com/google/googletest.git
   GIT_TAG master
		#    URL ${CMAKE_CURRENT_LIST_DIR}/googletest-master.zip
		#    URL_HASH SHA256=41f0f77e7752f903fe82835e7e81b55d5cf909948a59c204dddd4d4cd07d73dc
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/gtest
    CMAKE_ARGS
    -Dgtest_force_shared_crt=ON
    INSTALL_COMMAND "" # Disable install step
    )

#
# Introduce the `libgtest` target to make it usable as a real cmake target
# from our own cmakelists, no special cases and full build dependency-aware.
#
# This follows modern cmake practices, so the only command to actually use gtest is
#
#    target_link_libraries(my_test mylibs libgtest)
#
# No fiddling with includes.
#
# On the other hand, to keep it simple, we do not export libgtest_main, so your test has
# to contain its own main(), very simple, just 2 lines.
#
ExternalProject_Get_Property(build_gtest source_dir binary_dir)
#
# This hack is needed because unfortunately there is no way to build ExternalProject
# before the rest, so we cannot use cmake finders (since they are run at configuration
# time, they would run when gtest is not yet built :-(
# Cannot use CMAKE_FIND_LIBRARY_PREFIXES and CMAKE_FIND_LIBRARY_SUFFIXES because these are lists.
#
if (UNIX)
    set(_LIBGTEST_NAME libgtest.a)
elseif (WIN32)
    # I know, this is a hack.
    set(_LIBGTEST_NAME Debug/gtest.lib)
else ()
    message(FATAL_ERROR "Unknown platform, don't know how libraries are named.")
endif ()
set(_LIBGTEST_PATH "${binary_dir}/googlemock/gtest/${_LIBGTEST_NAME}")

add_library(libgtest STATIC IMPORTED)
set_target_properties(libgtest PROPERTIES IMPORTED_LOCATION ${_LIBGTEST_PATH})

# Cannot use INCLUDE_DIRECTORIES, must use INTERFACE_INCLUDE_DIRECTORIES instead
# https://cmake.org/cmake/help/v3.6/manual/cmake-buildsystem.7.html#transitive-usage-requirements
# https://gitlab.kitware.com/cmake/cmake/issues/15689
#
# Sigh another hack due to limitations of ExternalProject. If we use INTERFACE_INCLUDE_DIRECTORIES,
# set_target_properties() will complain that the directory doesn't exist, so we need to create it
# at configure time.
set(_LIBGTEST_INCLUDE_DIR "${source_dir}/googletest/include")
file(MAKE_DIRECTORY ${_LIBGTEST_INCLUDE_DIR})
set_target_properties(libgtest PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${_LIBGTEST_INCLUDE_DIR})

# On Linux (but not on Mac), need explicit link against pthreads.
find_package(Threads)
set_target_properties(libgtest PROPERTIES INTERFACE_LINK_LIBRARIES Threads::Threads)

# This is the trick to build the external project before we actually use it
add_dependencies(libgtest build_gtest)
