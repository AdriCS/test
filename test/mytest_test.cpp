#include "gtest/gtest.h"

#include "myclass.h"

TEST(MyClass, REPLACE_ME)
{
  ASSERT_TRUE(true);
}

TEST(MyClass, ConstructorWithValue)
{
  MyClass c{7};
  ASSERT_EQ(c.get(), 7);
}

TEST(MyClass, DEFAULT_GET)
{
  MyClass c;
  ASSERT_EQ(c.get(), -5555);
}

TEST(SubClass, GET) {
		Subclass s;
		ASSERT_EQ(s.get(), 9999);
}

TEST(MyClass, TESTTEST) {
  MyClass c{55};
  ASSERT_EQ(c.someOtherMethod(), 9876);
}

TEST(Another, Another) {
  Another a;
  ASSERT_EQ(a.another(), 1234);
}
int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

