#include <iostream>
#include <memory>

#include "myclass.h"

int main() {
  std::cout << "TEST" << std::endl;

  std::unique_ptr<MyClass> a = std::make_unique<MyClass>();
  MyClass b{123};
  std::unique_ptr<MyClass> c = std::make_unique<Subclass>();

  std::cout << a->get()  << " " << b.get() << " " << c->get() << std::endl;
  Another x;
  std::cout << x.another() << std::endl;
  return 0;
}

