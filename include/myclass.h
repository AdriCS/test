#ifndef _MYCLASS_H
#define _MYCLASS_H

class MyClass {
  public:
	constexpr explicit MyClass(int x_) : x{x_}
	{}
	constexpr MyClass() = default;
    virtual ~MyClass() = default;

	virtual int get() { return x; }

	int someOtherMethod() const noexcept;
  private:
	int x{-5555};
};

class Subclass: public MyClass {
  public:
	explicit Subclass():MyClass{-1}{}
	~Subclass() override = default;
    int get() override { return 9999; }
};

class Another {
public:
	Another() = default;
	~Another() = default;

	constexpr int another() const noexcept { return 1234; }
};

#endif

